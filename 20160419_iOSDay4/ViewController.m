//
//  ViewController.m
//  20160419_iOSDay4
//
//  Created by ChenSean on 4/19/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()
{
    NSMutableArray * list;
}

@end

@implementation ViewController

- (IBAction) unwind: (UIStoryboardSegue *)segue{
    if ([[segue identifier] isEqualToString:@"unwindMyVC"]) {
        SecondViewController *vc = [segue sourceViewController];
        NSLog(@"%@", vc.str);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // list = [NSMutableArray new];
    list = [[NSMutableArray alloc] init];
    
    [list addObject:@"台中"];
    [list addObject:@"台北"];
    [list addObject:@"高雄"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 二手回收記憶體空間，當 scroll bar 往下移讓第一格的記憶體空間讓其他人可以使用
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    // 上面是新的寫法，骨子李面還是下面那種判斷寫法
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    //    
    //    if (cell == nil) {
    //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    //    }
    
    // 儲存格要塞值
    // title
    cell.textLabel.text = [list objectAtIndex:indexPath.row];
    
    //    cell.detailTextLabel // subtitle
    return cell;
}

// 會不好判斷執行順序，所以另選方法實現做抓到使用者使用哪種選擇
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"%@", [list objectAtIndex:indexPath.row]);
//}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    if ([[segue identifier] isEqualToString:@"VC2SecondVC"]) {
        SecondViewController *vc = [segue destinationViewController];
        vc.str = [list objectAtIndex:indexPath.row];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
