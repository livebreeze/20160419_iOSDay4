//
//  AppDelegate.h
//  20160419_iOSDay4
//
//  Created by ChenSean on 4/19/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

