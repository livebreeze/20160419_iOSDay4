//
//  ViewController.h
//  20160419_iOSDay4
//
//  Created by ChenSean on 4/19/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;


@end

